<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\DA\OptimaModel;
use App\DA\CcanModel;

date_default_timezone_set("Asia/Makassar");
class OptimaController extends Controller
{
	public function nde($id){
		$data = CcanModel::show_single($id);
		return view('optima.making_nde', ['data' => $data]);
	}

	public function save_nde($id){

	}

	public function save_analisa_layak($id){

	}

	public function save_rekon($id){

	}

	public function save_ba_ut($id){

	}

	public function save_re_analisa_layak($id){

	}
}
