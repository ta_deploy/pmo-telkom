<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
date_default_timezone_set("Asia/Makassar");
class CcanModel
{
	public static function save_log($ket, $id, $step_id){
		DB::Table('log')->insert([
			'step_id' => $step_id,
			'project_id' => $id,
			'keterangan' => $ket,
			'created_by'=> Session::get('auth')->username,
			'created_at'=> strtotime('now')
		]);
	}

	public static function save_jt($req){
		$id_me = DB::Table('project')->insertGetId([
			'ticares_id' => $req->ticares_id,
			'input_tgl' => strtotime($req->tgl_input),
			'step_id' => 1,
			'customer' => $req->customer,
			'alamat' => $req->alamat,
			'pic' => $req->pic,
			'koor' => $req->koor,
			'layanan' => $req->type_service,
			'sto' => $req->sto_code
		]);
		self::save_log($req->ket, $id_me, 1);
	}

	public static function show_single($id){
		return DB::Table('project')->where('id', $id)->first();
	}

	public static function show_log($id){
		return DB::Table('log')->where('project_id', $id)->get();
	}

	public static function save_pt1($id, $step){
		DB::Table('project')->where('id', $id)->update([
			'status_pt1' => $step ]);
	}

	public static function step($id){
		Return DB::SELECT("SELECT SUBSTRING_INDEX( GROUP_CONCAT(CAST(keterangan as char) ORDER BY created_at DESC), ',', 1 ) AS open, project_id, keterangan, created_at, step_id FROM log WHERE project_id = $id GROUP BY step_id");
	}
}