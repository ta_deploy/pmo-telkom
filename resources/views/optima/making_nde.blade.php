@extends('layout')
@section('title', 'Making NDE')
@section('content')
@section('css')
<style type="text/css">
#map {
	height: 100%;
}
.row-eq-height {
	display: -webkit-box;
	display: -webkit-flex;
	display: -ms-flexbox;
	display: flex;	
}
</style>
@endsection
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="row row-eq-height">
		<div class="col-md-6">
			<div class="form-group form-group-sm">
				<label for="ticares_id" class="col-md-3 control-label">Ticares ID :</label>
				<div class="col-md-9">
					<label>{{ $data->ticares_id ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="tgl_input" class="col-md-3 control-label">Tanggal Input :</label>
				<div class="col-md-9">
					<label>{{ $data->input_tgl ? date('Y-m-d', $data->input_tgl) : '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="Customer" class="col-md-3 control-label">Customer :</label>
				<div class="col-md-9">
					<label>{{ $data->customer ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="Customer" class="col-md-3 control-label">Alamat :</label>
				<div class="col-md-9">
					<label>{{ $data->alamat ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="pic" class="col-md-3 control-label">PIC :</label>
				<div class="col-md-9">
					<label>{{ $data->pic ?: '' }}</label>
				</div>
			</div>
			<div class="form-group form-group-sm">
				<label for="koor" class="col-md-3 control-label">Koordinat :</label>
				<div class="col-md-9">
					<label>{{ $data->koor ?: '' }}</label>
				</div>
				<div id="map"></div>
			</div>
			<div class="form-group form-group-sm">
				<label for="type_service" class="col-md-3 control-label">Tipe Layanan :</label>
				<div class="col-md-9">
					<label>{{ $data->layanan ?: '' }}</label>
				</div>

			</div>
			<div class="form-group form-group-sm">
				<label for="sto_code" class="col-md-3 control-label">Kode STO :</label>
				<div class="col-md-9">
					<label>{{ $data->sto ?: '' }}</label>
				</div>
			</div>
		</div>
		<div class="sticy_bitchis col-md-6">
			<div class="content_optima" >
				<div class="form-group form-group-sm">
					<label for="bast" class="col-md-3 control-label">Upload Data Nota</label>
					<div class="col-md-4">
						<input type="file" name="bast" id="bast" value="">
					</div>
				</div>
				<div class="form-group form-group-sm">
					<label for="ket" class="col-md-3 control-label">Keterangan</label>
					<div class="col-md-9">
						<textarea rows="3" class="form-control" id="ket"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<button type="submit" class="btn btn-block"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
		</div>
	</div>
	
	<div class="row">
		<div id="log"></div>
	</div>
</form>
@endsection
@section('js')
<script type="text/javascript">
	$(function() {
		$.ajax({
			type: "GET",
			url: "/log",
			success: function(data){
				$('#log').html(data);
				$('thead, tbody tr').css({
					'display':'table',
					'width':'100%',
					'table-layout':'fixed'
				});
				$('thead').css({
					'width': 'calc( 100% - 1em )'
				});
			}
		});

		$(window).scroll(function() {
			var windowTop = $(window).scrollTop();
			if (0 < windowTop && $(".sticy_bitchis").height() > windowTop) {
				$('.content_optima').css({
					'position': '-webkit-sticky',
					'position': 'sticky',
					'top': '44px'
				});
			} else {
				$('.content_optima').css({
					'position': 'relative',
					'top': '0px'
				});
			}
		});
	});
</script>

@endsection