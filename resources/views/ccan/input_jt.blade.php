@extends('layout')
@section('title', 'Input JT')
@section('content')
@section('css')
<style type="text/css">
#map {
	height: 100%;
}
</style>
@endsection
<form class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="form-group form-group-sm">
		<label for="ticares_id" class="col-md-2 control-label">Ticares ID</label>
		<div class="col-md-10">
			<input type="text" name="ticares_id" class="form-control" id="ticares_id" value="">
		</div>
	</div>
	<div class="form-group form-group-sm">
		<label for="tgl_input" class="col-md-2 control-label">Tanggal Input</label>
		<div class="col-md-10">
			<input type="text" name="tgl_input" class="form-control" id="tgl_input" value="">
		</div>
	</div>
	<div class="form-group form-group-sm">
		<label for="Customer" class="col-md-2 control-label">Customer</label>
		<div class="col-md-3">
			<input type="text" name="customer" class="form-control" id="customer" value="">
		</div>
		<label for="Customer" class="col-md-2 control-label">Alamat</label>
		<div class="col-md-5">
			<textarea rows="3" class="form-control" id="alamat" name="alamat"></textarea>
		</div>
	</div>
	<div class="form-group form-group-sm">
		<label for="pic" class="col-md-2 control-label">PIC</label>
		<div class="col-md-10">
			<input type="text" name="pic" class="form-control" id="pic" value="">
		</div>
	</div>
	<div class="form-group form-group-sm">
		<label for="koor" class="col-md-2 control-label">Koordinat</label>
		<div class="col-md-4">
			<input type="text" name="koor" class="form-control" id="koor" value="">
		</div>
		<div id="map"></div>
	</div>
	<div class="form-group form-group-sm">
		<label for="type_service" class="col-md-2 control-label">Tipe Layanan</label>
		<div class="col-md-4">
			<input type="text" name="type_service" class="form-control" id="type_service" value="">
		</div>
		<label for="sto_code" class="col-md-2 control-label">Kode STO</label>
		<div class="col-md-4">
			<input type="text" name="sto_code" class="form-control" id="sto_code" value="">
		</div>
	</div>
	<div class="form-group form-group-sm">
		<label for="ket" class="col-md-2 control-label">Keterangan</label>
		<div class="col-md-10">
			<textarea rows="3" class="form-control" name="ket" id="ket"></textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-9">
			<button type="submit" class="btn btn-block"><span class="btn-label-icon left fa fa-database"></span>Submit</button>
		</div>
	</div>
</form>
@endsection
@section('js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBtVsZ6zH9se1k80lIInnh-65KSua3_F4"></script>
<script type="text/javascript">
	$(function() {
		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 8,
				center: {lat: -34.397, lng: 150.644}
			});
			var geocoder = new google.maps.Geocoder();

			document.getElementById('submit').addEventListener('click', function() {
				geocodeAddress(geocoder, map);
			});
		}

		function geocodeAddress(geocoder, resultsMap) {
			var address = document.getElementById('address').value;
			geocoder.geocode({'address': address}, function(results, status) {
				if (status === 'OK') {
					resultsMap.setCenter(results[0].geometry.location);
					var marker = new google.maps.Marker({
						map: resultsMap,
						position: results[0].geometry.location
					});
				} else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			});
		}
	});
</script>

@endsection